from flask_wtf import FlaskForm
from .keto_form import KetoExpandForm, KetoCheckForm, ManageAccountForm
from wtforms import SubmitField, SelectField, BooleanField
from wtforms.validators import DataRequired
from .keto_data import (
    SUBJECT_MAP,
    OBJECT_MAP,
    RELATION_MAP
)


class LoginForm(FlaskForm):
    username = SelectField(
        "Username", choices=["user01", "user02"], validators=[DataRequired()]
    )
    remember_me = BooleanField("Remember Me")
    submit = SubmitField("Sign In")


class AppUserSupportExpandForm(KetoExpandForm):
    keto_namespace = "app-user_support"
    object_choices = OBJECT_MAP["app-user_support"]
    relation_choices = {v: v for v in RELATION_MAP["app-user_support"]}


class AppUserSupportCheckForm(KetoCheckForm):
    keto_namespace = "app-user_support"
    object_choices = OBJECT_MAP["app-user_support"]
    subject_choices = SUBJECT_MAP
    relation_choices = {v: v for v in RELATION_MAP["app-user_support"]}


class AttributesExpandForm(KetoExpandForm):
    keto_namespace = "attributes"
    object_choices = OBJECT_MAP["attributes"]
    relation_choices = {v: v for v in RELATION_MAP["attributes"]}


class AppUserSupportManageUserForm(ManageAccountForm):
    keto_namespace = "app-user_support"
    keto_object = OBJECT_MAP["app-user_support"]["user_account"]
    subject_choices = SUBJECT_MAP
    relation_choices = {v: v for v in RELATION_MAP["app-user_support"]}


class AttributesCheckForm(KetoCheckForm):
    keto_namespace = "attributes"
    object_choices = OBJECT_MAP["attributes"]
    subject_choices = SUBJECT_MAP
    relation_choices = {v: v for v in RELATION_MAP["attributes"]}


class GroupsExpandForm(KetoExpandForm):
    keto_namespace = "groups"
    object_choices = OBJECT_MAP["groups"]
    relation_choices = {v: v for v in RELATION_MAP["groups"]}


class GroupsCheckForm(KetoCheckForm):
    keto_namespace = "groups"
    object_choices = OBJECT_MAP["groups"]
    subject_choices = SUBJECT_MAP
    relation_choices = {v: v for v in RELATION_MAP["groups"]}


class OrganizationsExpandForm(KetoExpandForm):
    keto_namespace = "organizations"
    object_choices = OBJECT_MAP["organizations"]
    relation_choices = {v: v for v in RELATION_MAP["organizations"]}


class OrganizationsCheckForm(KetoCheckForm):
    keto_namespace = "organizations"
    object_choices = OBJECT_MAP["organizations"]
    subject_choices = SUBJECT_MAP
    relation_choices = {v: v for v in RELATION_MAP["organizations"]}


class RolesExpandForm(KetoExpandForm):
    keto_namespace = "roles"
    object_choices = OBJECT_MAP["roles"]
    relation_choices = {v: v for v in RELATION_MAP["roles"]}


class RolesCheckForm(KetoCheckForm):
    keto_namespace = "roles"
    object_choices = OBJECT_MAP["roles"]
    subject_choices = SUBJECT_MAP
    relation_choices = {v: v for v in RELATION_MAP["roles"]}
