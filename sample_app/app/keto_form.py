import os
import requests
import pprint
from typing import Any
from flask_wtf import FlaskForm
from wtforms import SubmitField, SelectField, BooleanField
from wtforms.validators import DataRequired
from .keto_data import replace_uuid_with_name, select_field_choices


class KetoForm(FlaskForm):
    keto_host = os.environ["KETO_READ_REMOTE"]
    indent_depth = 2
    headers = {
        "Content-Type": "application/json",
        "Accept": "applicaiton/json",
    }

    @property
    def keto_namespace_name(self) -> str:
        clean_name: str = self.keto_namespace
        clean_name = clean_name.replace("-", ": ")
        clean_name = clean_name.replace("_", " ")
        return clean_name.title()

    def pretty(self, indata) -> str:
        return pprint.pformat(indata, indent=self.indent_depth)

    def for_human(self, indata) -> str:
        return replace_uuid_with_name(self.pretty(indata))

    def name_for_uuid(self, src) -> str:
        return replace_uuid_with_name(src)

    def get_check(self, namespace: str, object_id: str, relation: str, subject_id: str):
        url = f"{self.keto_host}/check"
        payload = {
            "namespace": namespace,
            "object": object_id,
            "relation": relation,
            "subject": subject_id,
            "max-depth": 10,
        }
        r = requests.get(url, params=payload, headers=self.headers)
        return r.json()

    def get_expand(self, namespace: str, object_id: str, relation: str):
        url = f"{self.keto_host}/expand"
        payload = {
            "namespace": namespace,
            "object": object_id,
            "relation": relation,
            "max-depth": 10,
        }
        r = requests.get(url, params=payload, headers=self.headers)
        return r.json()

    def get_relation_tuples(
        self,
        namespace: str,
        object_id: str = "",
        relation: str = "",
        subject_id: str = "",
    ):
        url = f"{self.keto_host}/check"
        payload = {
            "namespace": namespace,
            "max-depth": 10,
        }
        if object_id:
            payload["object"] = object_id
        if relation:
            payload["relation"] = relation
        if subject_id:
            payload["subject"] = subject_id

        r = requests.get(url, params=payload, headers=self.headers)
        return r.json()

    def allowed(self,  namespace: str, object_id: str, relation: str, subject_id: str) -> bool:
        response = self.get_check(namespace, object_id, relation, subject_id)
        return response.get('allowed', False)


class KetoRelationTuplesForm(KetoForm):
    keto_namespace: str
    object_choices: Any
    subject_choices: Any
    relation_choices: Any

    keto_object = SelectField("Object", validators=[DataRequired()])
    keto_subject = SelectField("Subject", validators=[DataRequired()])
    keto_relation = SelectField("Relation", validators=[DataRequired()])
    submit = SubmitField("Check")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.keto_object.choices = self.build_object_choices()
        self.keto_subject.choices = self.build_subject_choices()
        self.keto_relation.choices = self.build_relation_choices()

    @property
    def title(self) -> str:
        return ""

    def build_object_choices(self):
        return select_field_choices(self.object_choices)

    def build_subject_choices(self):
        return select_field_choices(self.subject_choices)

    def build_relation_choices(self):
        return select_field_choices(self.relation_choices)


class KetoCheckForm(KetoForm):
    keto_namespace: str
    object_choices: Any
    subject_choices: Any
    relation_choices: Any

    keto_subject = SelectField("Subject", validators=[DataRequired()])
    keto_relation = SelectField("Relation", validators=[DataRequired()])
    keto_object = SelectField("Object", validators=[DataRequired()])
    submit = SubmitField("Check")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.keto_object.choices = self.build_object_choices()
        self.keto_subject.choices = self.build_subject_choices()
        self.keto_relation.choices = self.build_relation_choices()

    @property
    def title(self) -> str:
        return ""

    def build_object_choices(self):
        return select_field_choices(self.object_choices)

    def build_subject_choices(self):
        return select_field_choices(self.subject_choices)

    def build_relation_choices(self):
        return select_field_choices(self.relation_choices)


class KetoExpandForm(KetoForm):
    keto_namespace: str
    object_choices: Any
    relation_choices: Any

    show_name_for_uuid = BooleanField("User names instead of UUIDs")
    keto_object = SelectField(
        "Object", validators=[DataRequired()], description="The entity acted upon"
    )
    keto_relation = SelectField("Relation", validators=[DataRequired()])
    submit = SubmitField("Expand")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.keto_object.choices = self.build_subject_choices()
        self.keto_relation.choices = self.build_relation_choices()

    @property
    def title(self) -> str:
        return ""

    def build_subject_choices(self):
        return select_field_choices(self.object_choices)

    def build_relation_choices(self):
        return select_field_choices(self.relation_choices)


class ManageAccountForm(KetoForm):
    keto_namespace: str
    keto_object: str
    subject_choices: Any
    relation_choices: Any

    keto_subject = SelectField("Authenticated User", validators=[DataRequired()])
    submit = SubmitField("Build Form")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.keto_subject.choices = self.build_subject_choices()

    @property
    def title(self) -> str:
        return ""

    def build_subject_choices(self):
        return select_field_choices(self.subject_choices)

    def relations_with_buttons(self):
        ignore = ["read_demographics", "update_demographics"]
        return [x for x in self.relation_choices if x not in ignore]

    def pretty_relation(self, relation: str):
        return relation.replace('_', ' ').title()
