from flask import render_template, flash, redirect, url_for
from app import app
from app.forms import (
    LoginForm,
    OrganizationsExpandForm,
    OrganizationsCheckForm,
    GroupsExpandForm,
    GroupsCheckForm,
    RolesExpandForm,
    RolesCheckForm,
    AttributesExpandForm,
    AttributesCheckForm,
    AppUserSupportExpandForm,
    AppUserSupportCheckForm,
    AppUserSupportManageUserForm
)


@app.route("/")
@app.route("/index")
def index():
    user = {"username": "user01", "uuid": "237988a0-d0be-5318-ad33-74bdbf8224ff"}
    return render_template("index.html", title="Home", user=user)


@app.route("/demo_form", methods=["GET", "POST"])
def demo_form():
    form = AppUserSupportManageUserForm()
    return render_template("demo_form.html", title="Manage User Account", form=form)


@app.route("/expand/app-user_support", methods=["GET", "POST"])
def app_user_support_expand():
    form = AppUserSupportExpandForm()
    return render_template("expand.html", title=form.title, form=form)


@app.route("/check/app-user_support", methods=["GET", "POST"])
def app_user_support_check():
    form = AppUserSupportCheckForm()
    return render_template("check.html", title=form.title, form=form)


@app.route("/expand/attribute", methods=["GET", "POST"])
def attribute_expand():
    form = AttributesExpandForm()
    return render_template("expand.html", title=form.title, form=form)


@app.route("/check/attribute", methods=["GET", "POST"])
def attribute_check():
    form = AttributesCheckForm()
    return render_template("check.html", title=form.title, form=form)


@app.route("/expand/groups", methods=["GET", "POST"])
def groups_expand():
    form = GroupsExpandForm()
    return render_template("expand.html", title=form.title, form=form)


@app.route("/check/groups", methods=["GET", "POST"])
def groups_check():
    form = GroupsCheckForm()
    return render_template("check.html", title=form.title, form=form)


@app.route("/expand/organizations", methods=["GET", "POST"])
def organizations_expand():
    form = OrganizationsExpandForm()
    return render_template("expand.html", title=form.title, form=form)


@app.route("/check/organizations", methods=["GET", "POST"])
def organizations_check():
    form = RolesCheckForm()
    return render_template("check.html", title=form.title, form=form)


@app.route("/expand/roles", methods=["GET", "POST"])
def roles_expand():
    form = RolesExpandForm()
    return render_template("expand.html", title=form.title, form=form)


@app.route("/check/roles", methods=["GET", "POST"])
def roles_check():
    form = RolesCheckForm()
    return render_template("check.html", title=form.title, form=form)
