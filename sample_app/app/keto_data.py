from typing import Dict, List
from pathlib import Path
import json

OBJECT_MAP: Dict[str, Dict[str, str]] = json.loads(Path("/var/keto/object_map.json").open("r").read())
SUBJECT_MAP: Dict[str, str] = json.loads(Path("/var/keto/subject_map.json").open("r").read())
RELATION_MAP: Dict[str, List[str]] = json.loads(Path("/var/keto/namespace_relations.json").open("r").read())


def replace_uuid_with_name(src) -> str:
    fixed = src
    for namespace, objects in OBJECT_MAP.items():
        for k, v in objects.items():
            fixed = fixed.replace(v, k)
    for k, v in SUBJECT_MAP.items():
        fixed = fixed.replace(v, k)
    return fixed


def select_field_choices(src):
    return [(src[k], k) for k in sorted(src.keys())]
