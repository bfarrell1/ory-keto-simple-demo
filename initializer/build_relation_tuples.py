#! env python
import re
from typing import Dict, Set, List
import sys
import uuid
import json
from pathlib import Path


BASE_DIR: Path = Path("/app")
VAR_DIR: Path = Path("/var/keto")
PROFILE_DIR: Path = BASE_DIR.joinpath("load_profiles")
OUTPUT_DIR: Path = BASE_DIR.joinpath("/var/keto")

UUID5_NAMESPACE: uuid.UUID = uuid.uuid5(uuid.NAMESPACE_DNS, "localdev.naic.org")


uuid_cache: Dict[str, str] = {}
object_map: Dict[str, Dict[str, str]] = {}
subject_map: Dict[str, str] = {}
namespace_relations: Dict[str, Set[str]] = {}


def uuid5(to_encode: str):
    if to_encode not in uuid_cache:
        uuid_cache[to_encode] = str(uuid.uuid5(UUID5_NAMESPACE, to_encode))
    return uuid_cache[to_encode]


is_tuple = re.compile(r".+:.+#.+")


def save_uuid_map():
    with VAR_DIR.joinpath("subject_map.json").open("w") as f:
        f.write(json.dumps(subject_map))

    with VAR_DIR.joinpath("object_map.json").open("w") as f:
        f.write(json.dumps(object_map))

    json_safe: Dict[str, List[str]] = {k: sorted(list(v)) for k, v in namespace_relations.items()}
    with VAR_DIR.joinpath("namespace_relations.json").open("w") as f:
        f.write(json.dumps(json_safe))


def write_namespace_file(profile):
    entry = '{{\n  "id": {},\n  "name": "{}"\n}}'
    namespace_file = VAR_DIR.joinpath("namespaces", f"{profile['namespace']}.json")
    with namespace_file.open("w") as f:
        f.write(entry.format(profile["namespace_id"], profile["namespace"]))


def add_object_relation(namespace, relation):
    if namespace not in namespace_relations:
        namespace_relations[namespace] = set()
    namespace_relations[namespace].add(relation)


def build_relation_tuples(profile):
    tuples = []
    rtuple: str = (
        '  {{"namespace": "{}", "object": "{}", "relation": "{}", "subject": "{}"}}'
    )
    namespace = profile["namespace"]
    for object_profile in profile["objects"]:
        for object_name, relation_set in object_profile.items():
            object_uuid = object_uuid5(namespace, object_name)
            for relation, subjects in relation_set.items():
                add_object_relation(namespace, relation)
                for subject in subjects:
                    subject_entry = build_subject_entry(subject)
                    tuples.append(
                        rtuple.format(
                            namespace, object_uuid, relation, subject_entry
                        )
                    )
    return tuples


def object_uuid5(namespace, object_name):
    if namespace not in object_map:
        object_map[namespace] = {}
    object_map[namespace][object_name] = uuid5(f"{namespace}.{object_name}")
    return object_map[namespace][object_name]


def build_subject_entry(subject):
    if type(subject) == list:
        # ["groups","jira_admin","owner"]
        object_uuid = object_uuid5(subject[0], subject[1])
        subject_entry = f"{subject[0]}:{object_uuid}#{subject[2]}"
    elif is_tuple.match(subject):
        subject_entry = subject
    else:
        subject_map[subject] = uuid5(subject)
        subject_entry = subject_map[subject]
    return subject_entry


def process_load_profiles() -> int:
    status: int = 0
    tuple_file: Path = OUTPUT_DIR.joinpath("relation-tuples", "auto_tuples.json")
    relation_tuples: List = []
    for file in list(PROFILE_DIR.glob("*.json")):
        with file.open("r") as f:
            profile = json.loads(f.read())
            write_namespace_file(profile)
            relation_tuples.extend(build_relation_tuples(profile))
    with tuple_file.open("w") as f:
        f.write("[\n")
        f.write(",\n".join(relation_tuples))
        f.write("\n]")
    save_uuid_map()
    return status


if __name__ == "__main__":
    sys.exit(process_load_profiles())
