# Ory/Keto Simple Demo
## Purpose/Scope

This demo application explores some capabilities of the Ory/Keto
REST **read** API.  In addition, there is a sample application that 
demonstrates how authorization with Ory/Keto could be used.

This is not an exhaustive exploration of all capabilities 
of Ory/Kety.

## How To Use
After cloning the project locally:
1. change to the keto_sample_env directory
1. Issue: `docker compose up --build -d`
1. Open a browser to http://localhost:8181/
1. Explore

## Reuse

While the sample application is a one off and of almost no use 
beyond this demonstration the following could be 
used in another project:
- docker-compose.yml
- ./initializer/*
- ./keto/*
- ./venv/*

## Quick Links
- [Ory/Keto Documentation](https://www.ory.sh/keto/docs/)
- [Ory/Keto Concepts](https://www.ory.sh/keto/docs/concepts/relation-tuples/)
- [Ory/Keto REST API](https://www.ory.sh/keto/docs/reference/rest-api)

## How to modify Ory/Keto demo data
All data loaded into the local Ory/Keto instance is stored in the
`./initializer/load_profiles/*.json` files.  Add or modify these files
to change the data loaded into the Ory/Keto instance.

After making any modifications simply rebuild the docker images.
```shell
docker compose down
docker compose up --build -d
```